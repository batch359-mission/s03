# Python has several structure to store collections or multiple items in a single variable

# Lists - similar to arrays

names = ["John", "Kenneth", "Kevin", "Luke", "Nueve"] #String list
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, False, True, True]

random_elements = [260, "Hello", True]

print(names)
print(programs)
print(durations)
print(truth_values)
print(random_elements)


# Getting the list size
# The number of elements in a list can be counted using the len() method.
print(len(programs))

# Accessing values
#Accessing the first item
print(programs[0])
# Accessing the last item
print(programs[-1])
# Accesing the second item
print(programs[1])

# Access a range of values
# list[start index: end index]
print(programs[0:2]) # will display index 0 and 1
print(programs[:2])

# Updating lists
print(f'Current value: {programs[2]}')
programs[2] = 'Short Courses'
print(f'Current values: {programs[2]}')


# Mini activity:
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in the following format:
	# Mary, Matthew, Tom, Anna, Thomas
	# 100, 85, 88,90, 75
		# The grade of Mary is 100
		# The grade of Matthew is 85


student_names = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]

grades = [100, 85, 88, 90, 75]

for i in range(len(student_names)):
	print(f"The grade of {student_names[i]} is {grades[i]}")


# List Manipulation
# append () - it will allow us to insert items to a list
programs.append('global')
print(programs)

# Delete the last item on the list
del programs[-1]
print(programs)

# Membership checks - the "in" keyword checks if the element is in the list
print(20 in durations) # true
print(500 in durations) # false

# Sorting list - sort() method, by default: ascending

print(student_names)
student_names.sort() #student_name.sort(reverse=false)
print(student_names)

student_names.sort(reverse=True)
print(student_names)


# Emptying the list - the clear() method is used to empty the contents of the lists

test_lists = [1, 2, 3, 4, 5]
print(test_lists)
test_lists.clear()
print(test_lists)



# Dictionaries are used to store data values in key:value pairs
person1 = {
	"name": "Brandon",
	"age": 28,
	"occupation": "Developer",
	"isWorking": True,
	"languages": ["Python", "JavaScript", "PHP"]
}

print(person1)
print(len(person1))

# Accessing the values in the dictionary
print(person1["name"])

# keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# values() method will return a list of all the values in the dictionary
print(person1.values())

# items() method will return each item in a dictionary, as a key-value pair in a list
print(person1.items())


# Adding key-value pair
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang"})

print(person1)

# Deleting entries
del person1["nationality"]
print(person1)

person1.pop("fave_food")
print(person1)


person2 = {
	"name" : "John",
	"age" : 20
}

print(person2)
person2.clear()
print(person2)


# Looping through dictionaries
for key in  person1:
	print(f"The value of {key} is {person1[key]}")


# Nested Dictionaries
person3 = {
	"name": "Monica",
	"age": 25,
	"occupation": "Developer",
	"isWorking": True,
	"languages": ["Python", "JavaScript", "PHP"]
}


employees = {
	"employee1": person1,
	"employee2": person3
}

print(employees)

# Functions are blocks of code that runs when called
# "def" keyword is used to create a function. Syntax:
# def <function_name> ():

# defines a function called my_greeting
def my_greeting():
	# code to be run when my_greeting is called back
	print("Hello User!")

my_greeting()


def greet_user(username):
	print(f"Hello {username}!")

greet_user("Nueve")
# greet user()

def greet_user1(username=""):
	print(f"Hello {username}!")

greet_user1()
greet_user1("Kate")



# return statements - "return" keyword allow functions to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(5,10)
print(sum)


# Lambda Functions
# Lambda function is a small anonymous function that can be used for callbacks
# lambda parameter : expression
greeting = lambda person : f'hello {person}'

# def greeting(person):
# 	print(f"Hello {person}")

print(greeting("Anthony"))

mult = lambda a, b : a * b
print(mult(5,6))


# Mini Activity:
# Create a function that get the square root of a number

num = float(input("Enter number: \n"))

square_root = lambda a : a ** 0.5

result = square_root(num)
print(f"The square root of {num} is {result}")

# Classes
# This serves as blueprints to describe the concept of objects

# class ClassName():

class Car():
	# properties
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		# other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# method
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print(f"Filling up the fuel tank")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

new_car = Car("Toyota", "Vios", "2019")
print(f"My car is a {new_car.brand} {new_car.model}")

new_car.fill_fuel()